# harmonyos-sliding-layer-lib
本项目是基于开源项目android-sliding-layer-lib进行harmonyos化的移植和开发的，可以通过项目标签以及
[github地址](https://github.com/microsoftarchive/android-sliding-layer-lib)

移植版本：源master1.2.5版本

## 项目介绍
### 项目名称：harmonyos-sliding-layer-lib
### 所属系列：harmonyos的第三方组件适配移植
### 功能：
    高度自定义的侧滑布局。

### 项目移植状态：完全移植
### 调用差异：基本没有使用差异，请参照demo使用
### 原项目Doc地址：https://github.com/microsoftarchive/android-sliding-layer-lib
### 编程语言：java

### 项目截图（涉及文件仅供demo测试使用）

![image首界面](art/oneDemo.png)
![image主界面](art/twoDemo.png)
![demo效果](art/demo.gif)

## 安装教程

#### 方案一  
 
可以先下载项目，将项目中的Library库提取出来放在所需项目中通过build配置
```Java
dependencies {
     implementation project(":Library")
}
```

#### 方案二

- 1.项目根目录的build.gradle中的repositories添加：
```groovy
    buildscript {
       repositories {
           ...
           mavenCentral()
       }
       ...
   }
   
   allprojects {
       repositories {
           ...
           mavenCentral()
       }
   }
```
- 2.开发者在自己的项目中添加依赖
```groovy
dependencies {
    implementation 'com.gitee.ts_ohos:sliding-layer-lib:1.0.0'
}
```

Usage
-----

Add the SlidingLayer to your layout

```xml
  <com.wunderlist.slidinglayer.SlidingLayer
    ohos:id="$+id:sliding_01"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:align_parent_right="true"
    ohos:below="$id:include_01"
    app:shadowDrawable="$graphic:sidebar_shadow"
    app:shadowSize="20fp"
    app:offsetDistance="20fp"
    app:previewOffsetDistance="20fp"
    app:stickTo="top|bottom|right|left"
    app:changeStateOnTap="true"
         />
```

Pass parameters from the home screen to the home screen

```java
   Intent nextIntent = new Intent();
              nextIntent.setParam("stick", stick);
              nextIntent.setParam("transformer", transformer);
              nextIntent.setParam("enable", enable);
              nextIntent.setParam("shadow", shadow);
              nextIntent.setParam("offset", offset);
              Operation operation = new Intent.OperationBuilder()
                  .withDeviceId("")
                  .withBundleName("com.slidinglayersample")
                  .withAbilityName("com.slidinglayersample.MainAbility")
                  .build();
              nextIntent.setOperation(operation);
              startAbility(nextIntent);
```

Sets the initial parameters for the sliding layout

```java
 
private void initState() {

    try {
      setupSlidingLayerPosition(stick);
    } catch (NotExistException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    setupSlidingLayerTransform(transformer);
    setupPreviewMode(enable);
    setupShadow(shadow);
    setupLayerOffset(offset);
    dependChange = offset;
    mSlidingLayer.setTouch(touch, dependChange);
    mSlidingLayer.test();
  }
```


Customize
---------

 programmatically


Sets the initial position of the slide layer
```java
 setupSlidingLayerPosition(stick);
```

Sets the animation when the slide layer slides out
```java
setupSlidingLayerTransform(transformer);
```

Set whether to enable the preview module
```java
 setupPreviewMode(enable);
```

Set whether to enable inner shadow
```java
 setupShadow(shadow);
```

Sets whether to enable the offset of the slide layer
```java
setupLayerOffset(offset);
```


Code of Conduct
--------

       This project has adopted the Microsoft Open Source Code of Conduct. 
       For more information see the Code of Conduct FAQ or contact opencode@microsoft.com with any additional questions or comments.
    
