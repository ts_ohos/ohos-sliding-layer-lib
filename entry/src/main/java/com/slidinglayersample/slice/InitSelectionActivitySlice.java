/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.slidinglayersample.slice;

import com.slidinglayersample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.service.Window;
import ohos.bundle.AbilityInfo;
import ohos.multimodalinput.event.TouchEvent;

public class InitSelectionActivitySlice extends AbilitySlice {
  private DirectionalLayout directionalLayout01;
  private Button button01;
  private Button buttonText01;
  private DirectionalLayout directionalLayout02;
  private Button button02;
  private Button buttonText02;
  private Button button03;
  private RadioContainer radio01;
  private RadioContainer radio02;
  private RadioButton radioButton1;
  private RadioButton radioButton2;
  private RadioButton radioButton3;
  private RadioButton radioButton4;
  private RadioButton radioButton01;
  private RadioButton radioButton02;
  private RadioButton radioButton03;
  private RadioButton radioButton04;
  private Button oneButton;
  private Button twoButton;
  private Button checkButton01;
  private Button checkButton02;
  private Button checkButton03;
  private DirectionalLayout checkBox01;
  private DirectionalLayout checkBox02;
  private DirectionalLayout checkBox03;
  private Checkbox box01;
  private Checkbox box02;
  private Checkbox box03;
  private Component oneLayout;
  private Component twoLayout;
  private int oneCheck = 0;
  private int twoCheck = 0;
  private CommonDialog oneCommonDialog;
  private CommonDialog twoCommonDialog;
  private String stick;
  private String transformer;
  private boolean enable = false;
  private boolean shadow = false;
  private boolean offset = false;
  private Paint paint;
  private AnimatorValue animatorValue;
  private int heightPixels;
  private int widthPixels;

  @Override
  public void onStart(Intent intent) {
    super.onStart(intent);
    super.setUIContent(ResourceTable.Layout_ability_setting);
    Window window = getWindow();
    RgbColor rgbColor = new RgbColor(0, 121, 107);
    window.setStatusBarColor(rgbColor.asArgbInt());
    window.setNavigationBarColor(Color.BLACK.getValue());
    directionalLayout01 =
        (DirectionalLayout) findComponentById(ResourceTable.Id_button_direction01);
    button01 = (Button) findComponentById(ResourceTable.Id_button_01);
    buttonText01 = (Button) findComponentById(ResourceTable.Id_button_text_01);
    directionalLayout02 =
        (DirectionalLayout) findComponentById(ResourceTable.Id_button_direction02);
    button02 = (Button) findComponentById(ResourceTable.Id_button_02);
    buttonText02 = (Button) findComponentById(ResourceTable.Id_button_text_02);
    button03 = (Button) findComponentById(ResourceTable.Id_button_03);
    checkBox01 = (DirectionalLayout) findComponentById(ResourceTable.Id_checkBox01);
    checkBox02 = (DirectionalLayout) findComponentById(ResourceTable.Id_checkBox02);
    checkBox03 = (DirectionalLayout) findComponentById(ResourceTable.Id_checkBox03);
    checkButton01 = (Button) findComponentById(ResourceTable.Id_check_button01);
    checkButton02 = (Button) findComponentById(ResourceTable.Id_check_button02);
    checkButton03 = (Button) findComponentById(ResourceTable.Id_check_button03);
    box01 = (Checkbox) findComponentById(ResourceTable.Id_check_box01);
    box02 = (Checkbox) findComponentById(ResourceTable.Id_check_box02);
    box03 = (Checkbox) findComponentById(ResourceTable.Id_check_box03);
    oneLayout = LayoutScatter.getInstance(getContext())
        .parse(ResourceTable.Layout_radio_one, null, false);
    radio01 = (RadioContainer) oneLayout.findComponentById(ResourceTable.Id_radio_1);
    oneCommonDialog = new CommonDialog(getContext());
    oneCommonDialog.setContentCustomComponent(oneLayout);
    oneCommonDialog.setAutoClosable(true);
    oneCommonDialog.setDestroyedListener(new CommonDialog.DestroyedListener() {
      @Override
      public void onDestroy() {
        oneLayout = null;
        if (oneLayout == null) {
          oneLayout = LayoutScatter.getInstance(getContext())
              .parse(ResourceTable.Layout_radio_one, null, false);
          radio01 = (RadioContainer) oneLayout.findComponentById(ResourceTable.Id_radio_1);
          oneCommonDialog.setContentCustomComponent(oneLayout);
          radioButton();
          radioChange();
          radio01.mark(oneCheck);
        }
      }
    });
    twoLayout = LayoutScatter.getInstance(getContext())
        .parse(ResourceTable.Layout_radio_two, null, false);
    radio02 = (RadioContainer) twoLayout.findComponentById(ResourceTable.Id_radio_02);
    twoCommonDialog = new CommonDialog(getContext());
    twoCommonDialog.setContentCustomComponent(twoLayout);
    twoCommonDialog.setAutoClosable(true);
    twoCommonDialog.setDestroyedListener(new CommonDialog.DestroyedListener() {
      @Override
      public void onDestroy() {
        twoLayout = null;
        if (twoLayout == null) {
          twoLayout = LayoutScatter.getInstance(getContext())
              .parse(ResourceTable.Layout_radio_two, null, false);
          radio02 = (RadioContainer) twoLayout.findComponentById(ResourceTable.Id_radio_02);
          twoCommonDialog.setContentCustomComponent(twoLayout);
          radioButton();
          radioChange();
          radio02.mark(twoCheck);
        }
      }
    });
    radioChange();
    buttonClicked();
    boxcheck();

  }

  private void radioChange() {
    radio01.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
      @Override
      public void onCheckedChanged(RadioContainer radioContainer, int index) {
        oneCheck = radio01.getMarkedButtonId();
        choice();
        buttonText01.setText(stick);
        oneCommonDialog.hide();
      }
    });

    radio02.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
      @Override
      public void onCheckedChanged(RadioContainer radioContainer, int index) {
        twoCheck = radio02.getMarkedButtonId();
        choice();
        if (twoCheck == 3) {
          buttonText02.setText("Slide Joy!");
        } else {
          buttonText02.setText(transformer);
        }
        twoCommonDialog.hide();
      }
    });
  }

  private void buttonClicked() {
    radioButton();
    choice();
    radio01.mark(oneCheck);
    radio02.mark(twoCheck);
    buttonText01.setText(stick);
    if ("Slide".equals(transformer)) {
      buttonText02.setText("Slide Joy!");
    } else {
      buttonText02.setText(transformer);
    }


    directionalLayout01.setTouchEventListener(new Component.TouchEventListener() {
      @Override
      public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        int buttonX = directionalLayout01.getWidth();
        int buttonY = directionalLayout01.getHeight();
        switch (touchEvent.getAction()) {
          case TouchEvent.PRIMARY_POINT_DOWN:
            setAnimator(buttonX / 2, buttonY / 2, Color.GRAY, false, "directionalLayout01");
            break;
          case TouchEvent.PRIMARY_POINT_UP:
            setAnimator(buttonX / 2, buttonY / 2, Color.WHITE, true, "directionalLayout01");
            oneCommonDialog.show();
            break;
          default:
            break;
        }
        return true;
      }
    });
    directionalLayout02.setTouchEventListener(new Component.TouchEventListener() {
      @Override
      public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        int buttonX = directionalLayout02.getWidth();
        int buttonY = directionalLayout02.getHeight();
        switch (touchEvent.getAction()) {
          case TouchEvent.PRIMARY_POINT_DOWN:
            setAnimator(buttonX / 2, buttonY / 2, Color.GRAY, false, "directionalLayout02");
            break;
          case TouchEvent.PRIMARY_POINT_UP:
            setAnimator(buttonX / 2, buttonY / 2, Color.WHITE, true, "directionalLayout02");
            twoCommonDialog.show();
            break;
          default:
            break;
        }
        return true;
      }
    });

    checkBox01.setTouchEventListener(new Component.TouchEventListener() {
      @Override
      public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        int buttonX = checkBox01.getWidth();
        int buttonY = checkBox01.getHeight();

        switch (touchEvent.getAction()) {
          case TouchEvent.PRIMARY_POINT_DOWN:
            setAnimator(buttonX / 2, buttonY / 2, Color.GRAY, false, "checkBox01");
            break;
          case TouchEvent.PRIMARY_POINT_UP:
            setAnimator(buttonX / 2, buttonY / 2, Color.WHITE, true, "checkBox01");

            break;
          default:
            break;
        }
        return true;
      }
    });
    checkButton01.setClickedListener(new Component.ClickedListener() {
      @Override
      public void onClick(Component component) {
        box01.toggle();
      }
    });
    checkBox02.setTouchEventListener(new Component.TouchEventListener() {
      @Override
      public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        int buttonX = checkBox02.getWidth();
        int buttonY = checkBox02.getHeight();
        switch (touchEvent.getAction()) {
          case TouchEvent.PRIMARY_POINT_DOWN:
            setAnimator(buttonX / 2, buttonY / 2, Color.GRAY, false, "checkBox02");
            break;
          case TouchEvent.PRIMARY_POINT_UP:
            setAnimator(buttonX / 2, buttonY / 2, Color.WHITE, true, "checkBox02");

            break;
          default:
            break;
        }
        return true;
      }
    });
    checkButton02.setClickedListener(new Component.ClickedListener() {
      @Override
      public void onClick(Component component) {
        box02.toggle();
      }
    });
    checkBox03.setTouchEventListener(new Component.TouchEventListener() {
      @Override
      public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        int buttonX = checkBox03.getWidth();
        int buttonY = checkBox03.getHeight();

        switch (touchEvent.getAction()) {
          case TouchEvent.PRIMARY_POINT_DOWN:
            setAnimator(buttonX / 2, buttonY / 2, Color.GRAY, false, "checkBox03");
            break;
          case TouchEvent.PRIMARY_POINT_UP:
            setAnimator(buttonX / 2, buttonY / 2, Color.WHITE, true, "checkBox03");

            break;
          default:
            break;
        }
        return true;
      }
    });
    checkButton03.setClickedListener(new Component.ClickedListener() {
      @Override
      public void onClick(Component component) {
        box03.toggle();
      }
    });
    button03.setTouchEventListener(new Component.TouchEventListener() {
      @Override
      public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        int buttonX = button03.getWidth();
        int buttonY = button03.getHeight();

        switch (touchEvent.getAction()) {
          case TouchEvent.PRIMARY_POINT_DOWN:
            setAnimator(buttonX / 2, buttonY / 2, Color.GRAY, false, "button03");
            break;
          case TouchEvent.PRIMARY_POINT_UP:
            setAnimator(buttonX / 2, buttonY / 2, Color.WHITE, true, "button03");
            choice();
            Intent nextIntent = new Intent();
            nextIntent.setParam("stick", stick);
            nextIntent.setParam("transformer", transformer);
            nextIntent.setParam("enable", enable);
            nextIntent.setParam("shadow", shadow);
            nextIntent.setParam("offset", offset);
            Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(getContext().getBundleName())
                .withAbilityName("com.slidinglayersample.MainAbility")
                .build();
            nextIntent.setOperation(operation);
            startAbility(nextIntent);
            break;
          default:
            break;
        }
        return true;
      }
    });

  }

  private void radioButton() {
    radioButton1 = (RadioButton) oneLayout.findComponentById(ResourceTable.Id_radio_button_1);
    radioButton2 = (RadioButton) oneLayout.findComponentById(ResourceTable.Id_radio_button_2);
    radioButton3 = (RadioButton) oneLayout.findComponentById(ResourceTable.Id_radio_button_3);
    radioButton4 = (RadioButton) oneLayout.findComponentById(ResourceTable.Id_radio_button_4);
    radioButton01 = (RadioButton) twoLayout.findComponentById(ResourceTable.Id_radio_button_01);
    radioButton02 = (RadioButton) twoLayout.findComponentById(ResourceTable.Id_radio_button_02);
    radioButton03 = (RadioButton) twoLayout.findComponentById(ResourceTable.Id_radio_button_03);
    radioButton04 = (RadioButton) twoLayout.findComponentById(ResourceTable.Id_radio_button_04);
    oneButton = (Button) oneLayout.findComponentById(ResourceTable.Id_radio_one_button);
    twoButton = (Button) twoLayout.findComponentById(ResourceTable.Id_radio_two_button);

    radioButton1.setTouchEventListener(new Component.TouchEventListener() {
      @Override
      public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        int radioX = radioButton1.getWidth();
        int radioY = radioButton1.getHeight();
        switch (touchEvent.getAction()) {
          case TouchEvent.PRIMARY_POINT_DOWN:
            setAnimator(radioX / 2, radioY / 2, Color.GRAY, false, "radioButton1");
            break;
          case TouchEvent.PRIMARY_POINT_UP:
            setAnimator(radioX / 2, radioY / 2, Color.WHITE, true, "radioButton1");
            oneCommonDialog.hide();
            break;
          default:
            break;
        }
        return true;
      }
    });
    radioButton2.setTouchEventListener(new Component.TouchEventListener() {
      @Override
      public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        int radioX = radioButton2.getWidth();
        int radioY = radioButton2.getHeight();
        switch (touchEvent.getAction()) {
          case TouchEvent.PRIMARY_POINT_DOWN:
            setAnimator(radioX / 2, radioY / 2, Color.GRAY, false, "radioButton2");
            break;
          case TouchEvent.PRIMARY_POINT_UP:
            setAnimator(radioX / 2, radioY / 2, Color.WHITE, true, "radioButton2");
            oneCommonDialog.hide();
            break;
          default:
            break;
        }
        return true;
      }
    });
    radioButton3.setTouchEventListener(new Component.TouchEventListener() {
      @Override
      public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        int radioX = radioButton3.getWidth();
        int radioY = radioButton3.getHeight();

        switch (touchEvent.getAction()) {
          case TouchEvent.PRIMARY_POINT_DOWN:
            setAnimator(radioX / 2, radioY / 2, Color.GRAY, false, "radioButton3");
            break;
          case TouchEvent.PRIMARY_POINT_UP:
            setAnimator(radioX / 2, radioY / 2, Color.WHITE, true, "radioButton3");
            oneCommonDialog.hide();
            break;
          default:
            break;
        }
        return true;
      }
    });
    radioButton4.setTouchEventListener(new Component.TouchEventListener() {
      @Override
      public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        int radioX = radioButton4.getWidth();
        int radioY = radioButton4.getHeight();
        switch (touchEvent.getAction()) {
          case TouchEvent.PRIMARY_POINT_DOWN:
            setAnimator(radioX / 2, radioY / 2, Color.GRAY, false, "radioButton4");
            break;
          case TouchEvent.PRIMARY_POINT_UP:
            setAnimator(radioX / 2, radioY / 2, Color.WHITE, true, "radioButton4");
            oneCommonDialog.hide();
            break;
          default:
            break;
        }
        return true;
      }
    });
    radioButton01.setTouchEventListener(new Component.TouchEventListener() {
      @Override
      public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        int radioX = radioButton01.getWidth();
        int radioY = radioButton01.getHeight();

        switch (touchEvent.getAction()) {
          case TouchEvent.PRIMARY_POINT_DOWN:
            setAnimator(radioX / 2, radioY / 2, Color.GRAY, false, "radioButton01");
            break;
          case TouchEvent.PRIMARY_POINT_UP:
            setAnimator(radioX / 2, radioY / 2, Color.WHITE, true, "radioButton01");
            twoCommonDialog.hide();
            break;
          default:
            break;
        }
        return true;
      }
    });
    radioButton02.setTouchEventListener(new Component.TouchEventListener() {
      @Override
      public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        int radioX = radioButton02.getWidth();
        int radioY = radioButton02.getHeight();

        switch (touchEvent.getAction()) {
          case TouchEvent.PRIMARY_POINT_DOWN:
            setAnimator(radioX / 2, radioY / 2, Color.GRAY, false, "radioButton02");
            break;
          case TouchEvent.PRIMARY_POINT_UP:
            setAnimator(radioX / 2, radioY / 2, Color.WHITE, true, "radioButton02");
            twoCommonDialog.hide();
            break;
          default:
            break;
        }
        return true;
      }
    });
    radioButton03.setTouchEventListener(new Component.TouchEventListener() {
      @Override
      public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        int radioX = radioButton03.getWidth();
        int radioY = radioButton03.getHeight();
        switch (touchEvent.getAction()) {
          case TouchEvent.PRIMARY_POINT_DOWN:
            setAnimator(radioX / 2, radioY / 2, Color.GRAY, false, "radioButton03");
            break;
          case TouchEvent.PRIMARY_POINT_UP:
            setAnimator(radioX / 2, radioY / 2, Color.WHITE, true, "radioButton03");
            twoCommonDialog.hide();
            break;
          default:
            break;
        }
        return true;
      }
    });
    radioButton04.setTouchEventListener(new Component.TouchEventListener() {
      @Override
      public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        int radioX = radioButton04.getWidth();
        int radioY = radioButton04.getHeight();
        switch (touchEvent.getAction()) {
          case TouchEvent.PRIMARY_POINT_DOWN:
            setAnimator(radioX / 2, radioY / 2, Color.GRAY, false, "radioButton04");
            break;
          case TouchEvent.PRIMARY_POINT_UP:
            setAnimator(radioX / 2, radioY / 2, Color.WHITE, true, "radioButton04");
            twoCommonDialog.hide();
            break;
          default:
            break;
        }
        return true;
      }
    });
    oneButton.setTouchEventListener(new Component.TouchEventListener() {
      @Override
      public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        int radioX = oneButton.getWidth();
        int radioY = oneButton.getHeight();
        switch (touchEvent.getAction()) {
          case TouchEvent.PRIMARY_POINT_DOWN:
            setAnimator(radioX / 2, radioY / 2, Color.GRAY, false, "oneButton");
            break;
          case TouchEvent.PRIMARY_POINT_UP:
            setAnimator(radioX / 2, radioY / 2, Color.WHITE, true, "oneButton");
            oneCommonDialog.hide();
            break;
          default:
            break;
        }
        return true;
      }
    });
    twoButton.setTouchEventListener(new Component.TouchEventListener() {
      @Override
      public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        int radioX = twoButton.getWidth();
        int radioY = twoButton.getHeight();
        switch (touchEvent.getAction()) {
          case TouchEvent.PRIMARY_POINT_DOWN:
            setAnimator(radioX / 2, radioY / 2, Color.GRAY, false, "twoButton");
            break;
          case TouchEvent.PRIMARY_POINT_UP:
            setAnimator(radioX / 2, radioY / 2, Color.WHITE, true, "twoButton");
            twoCommonDialog.hide();
            break;
          default:
            break;
        }
        return true;
      }
    });
  }

  private void choice() {
    switch (oneCheck) {
      case 1:
        stick = "Left";
        break;
      case 2:
        stick = "Top";
        break;
      case 3:
        stick = "Bottom";
        break;
      default:
        stick = "Right";
        break;
    }
    switch (twoCheck) {
      case 1:
        transformer = "Alpha";
        break;
      case 2:
        transformer = "Rotation";
        break;
      case 3:
        transformer = "Slide";
        break;
      default:
        transformer = "None";
        break;
    }
  }

  private void initializeChoice() {
    switch (stick) {
      case "Left":
        oneCheck = 1;
        break;
      case "Top":
        oneCheck = 2;
        break;
      case "Bottom":
        oneCheck = 3;
        break;
      default:
        oneCheck = 0;
        break;
    }
    switch (transformer) {
      case "Alpha":
        twoCheck = 1;
        break;
      case "Rotation":
        twoCheck = 2;
        break;
      case "Slide":
        twoCheck = 3;
        break;
      default:
        twoCheck = 0;
        break;
    }
    box01.setChecked(enable);
    box02.setChecked(shadow);
    box03.setChecked(offset);
  }

  /**
   * Checks whether the three options are selected
   */
  private void boxcheck() {
    box01.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
      @Override
      public void onCheckedChanged(AbsButton absButton, boolean b) {
        enable = box01.isChecked();
      }
    });
    box02.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
      @Override
      public void onCheckedChanged(AbsButton absButton, boolean b) {
        shadow = box02.isChecked();

      }
    });
    box03.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
      @Override
      public void onCheckedChanged(AbsButton absButton, boolean b) {
        offset = box03.isChecked();
      }
    });
  }

  private void changeBackgroundColorTemp(int x, int y,
                                         int radius, Color color, boolean up, String name) {
    Component.DrawTask drawTask = new Component.DrawTask() {
      @Override
      public void onDraw(Component component, Canvas canvas) {
        paint = null;
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL_STYLE);
        paint.setColor(color);
        if (up) {
          paint.setAlpha(0.0f);
        } else {
          paint.setAlpha(0.2f);
        }
        canvas.drawCircle(x, y, radius, paint);
      }
    };
    switch (name) {
      case "directionalLayout01":
        directionalLayout01.addDrawTask(drawTask);
        break;
      case "directionalLayout02":
        directionalLayout02.addDrawTask(drawTask);
        break;
      case "radioButton1":
        radioButton1.addDrawTask(drawTask);
        break;
      case "radioButton2":
        radioButton2.addDrawTask(drawTask);
        break;
      case "radioButton3":
        radioButton3.addDrawTask(drawTask);
        break;
      case "radioButton4":
        radioButton4.addDrawTask(drawTask);
        break;
      case "radioButton01":
        radioButton01.addDrawTask(drawTask);
        break;
      case "radioButton02":
        radioButton02.addDrawTask(drawTask);
        break;
      case "radioButton03":
        radioButton03.addDrawTask(drawTask);
        break;
      case "radioButton04":
        radioButton04.addDrawTask(drawTask);
        break;
      case "oneButton":
        oneButton.addDrawTask(drawTask);
        break;
      case "twoButton":
        twoButton.addDrawTask(drawTask);
        break;
      case "checkBox01":
        checkBox01.addDrawTask(drawTask);
        break;
      case "checkBox02":
        checkBox02.addDrawTask(drawTask);
        break;
      case "checkBox03":
        checkBox03.addDrawTask(drawTask);
        break;
      case "button03":
        button03.addDrawTask(drawTask);
        break;
      default:
        break;
    }
  }

  private void setAnimator(int x, int y, Color color, boolean up, String name) {
    animatorValue = null;
    animatorValue = new AnimatorValue();
    animatorValue.setCurveType(
        Animator.CurveType.CUBIC_BEZIER_EXTREME_DECELERATION);
    animatorValue.setDuration(1500);
    if (up) {
      animatorValue.setDelay(500);
    }
    animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
      @Override
      public void onUpdate(AnimatorValue animatorValue, float v) {
        changeBackgroundColorTemp(x, y, (int) (2 * x * v), color, up, name);
      }
    });
    animatorValue.start();
  }

  @Override
  public void onActive() {
    super.onActive();
  }

  @Override
  public void onForeground(Intent intent) {
    super.onForeground(intent);
  }

}
