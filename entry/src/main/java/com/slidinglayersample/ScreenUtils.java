/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.slidinglayersample;

import java.util.Optional;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;


/**
 * 获取屏幕宽高等信息、全屏切换、保持屏幕常亮、截屏等
 *
 * @author matt
 * blog: addapp.cn
 */
public final class ScreenUtils {
  private static final String TAG = "ScreenUtils";
  private static DisplayAttributes dm = null;

  public static DisplayAttributes displayMetrics(Context context) {
    if (null != dm) {
      return dm;
    }
    Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(context);
    dm = display.get().getAttributes();
    return dm;
  }

  public static int widthPixels(Context context) {
    return displayMetrics(context).width;
  }

  public static int heightPixels(Context context) {
    return displayMetrics(context).height;
  }

}
